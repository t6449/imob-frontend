# IMOB ReactJS App
Se trata de una aplicacion con el objetivo de servir de apoyo en el negocio de las inmobiliarias.
Permitira la administracion de diferentes tipos de propiedades, presentando principal foco en el
inventario.

El factor diferencial respecto de soluciones desktop o web ya existentes es que la misma sera una
solucion a medida, utilizando la mejores practicas de desarrollo agil.
Otro factor es que la misma tendra soporte para una alta demanda de usuarios concurrentes.

## Run the App in Local Machine
 Install the required dependencies
```
    yarn install
```

Start the application.
```
    yarn start
```
