import React from 'react';
import {BrowserRouter, Route} from 'react-router-dom';
import HomeScreen from '../screens/home/HomeScreen';
import HeaderApp from '../header/HeaderApp';
import InventoryScreen from "../screens/inventory/InventoryScreen";
import PropertyScreen from '../screens/property-crud/PropertyScreen';
import {PropertyEdit} from "../screens/property-crud/property-add-edit/property-edit/PropertyEdit";
import {PropertyAdd} from "../screens/property-crud/property-add-edit/property-add/PropertyAdd";
import {ContainerMain} from "../shared/styles/ContainerStyles";
import './router.css'

const Router = () => {
    return (
        <BrowserRouter>
            <div id="root-container">
                <div id="wrapper-main-header">
                    <HeaderApp/>
                </div>
                <ContainerMain id="main-content">
                    <Route path='/' component={HomeScreen} exact/>
                    <Route path='/inventory' component={InventoryScreen} exact/>
                    <Route path='/properties' component={PropertyScreen} exact/>
                    <Route path='/properties/edit/:id' component={PropertyEdit}/>
                    <Route path='/properties/add' component={PropertyAdd}/>
                </ContainerMain>
            </div>
        </BrowserRouter>
    );
};

export default Router;
