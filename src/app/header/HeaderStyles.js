import styled from 'styled-components';

export const Container = styled.div`
  position: fixed;
  top: 0;
  width: 100%;
  z-index: 999;

  .header-imob {
    background: #141414;
    border: 0;
  }

  .brand-imob {
    font-family: "Serif", system-ui;
    font-size: 2em !important;
    font-weight: 800;
    clip-path: polygon(0 30%, 100% 11%, 100% 75%, 0 92%);
    padding: 5px;
  }

  .brand-imob:hover {
    color: #dfdfdf !important;
  }

  .ant-menu-dark .ant-menu-item{
    color: rgba(255, 255, 255, 0.85);
  }
  
`;
