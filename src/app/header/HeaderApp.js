import React, {useEffect, useState} from 'react';
import './HeaderStyles';
import {useHistory, useLocation} from 'react-router-dom';
import {Header} from 'antd/es/layout/layout';
import {Menu} from 'antd';
import {Container} from "./HeaderStyles";

const HeaderApp = () => {

    const history = useHistory();

    const location = useLocation();
    const { pathname }  = location;

    const [selectedKeys, setSelectedKeys] = useState(["/"]);

    const keys = {
        home: "/",
        inventory: "/inventory",
        properties: "/properties",
    };

    useEffect(() => {
        if (Object.values(keys).includes(pathname))
        {
            setSelectedKeys([pathname])
        }

    }, [pathname])

    return (
        <Container>
            <Header>
                <Menu
                    theme='dark'
                    mode='horizontal'
                    selectable={true}
                    selectedKeys={selectedKeys}
                >
                    <Menu.Item
                        key={keys.home}
                        onClick={() => history.push(keys.home)}
                    >
                        Inicio
                    </Menu.Item>
                    <Menu.Item
                        key={keys.properties}
                        onClick={() => history.push(keys.properties)}
                    >
                        Propiedades ABM
                    </Menu.Item>
                    <Menu.Item
                        key={keys.inventory}
                        onClick={() => history.push(keys.inventory)}
                    >
                        Inventario
                    </Menu.Item>
                </Menu>
            </Header>
        </Container>
    );
};

export default HeaderApp;
