import React from 'react';

import {Spin} from "antd";
import {LoadingOutlined} from '@ant-design/icons';
import {Container} from "./LoadingStyles";

const Loading = ({show}) => {

    const antIcon = <LoadingOutlined style={{fontSize: 128}} spin/>;

    return (

        <Container>
            <Spin
                indicator={antIcon}
                size={"large"}
                spinning={show}
            />
        </Container>
    );
};

export default Loading;
