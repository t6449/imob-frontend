
export const BACKEND_BASE_URL = process.env.REACT_APP_BACKEND_BASE_URL || "http://localhost:18080";

// Drive
const DRIVE = "/drive/api/v1"
export const ENDPOINT_FILES = BACKEND_BASE_URL + DRIVE + "/files";

// Inventory
const INVENTORY = "/inventory/api/v1"
export const ENDPOINT_CLASSIFICATIONS = BACKEND_BASE_URL + INVENTORY + "/classifications";
export const ENDPOINT_PROPERTIES = BACKEND_BASE_URL + INVENTORY + "/properties";
