import {toast} from "react-hot-toast";

const DELAY = 5000;

// Facade
export const toastSuccess = (msg = '') => {
    toast.success(msg, {
            duration: DELAY,
            position: 'top-right',
    });
}

export const toastError = (msg = '') => {
    toast.error(msg, {
        duration: DELAY,
        position: 'top-right',
    });
}
