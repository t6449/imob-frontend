import {SEARCH_HOME} from "./actionTypes";

export const stateDefault = {
    text: ''
}
export const homeReducer = (state = stateDefault, action) => {
    switch (action.type) {
        case SEARCH_HOME:
            return {...state, text: action.payload};
        default:
            return state;
    }
};
