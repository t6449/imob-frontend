import { SEARCH_HOME } from "./actionTypes";

export const searchHomeAction = (text = '') => async (dispatch) => {
    dispatch({ type: SEARCH_HOME, payload: text});
};
