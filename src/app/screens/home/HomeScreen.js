import React, {useEffect} from 'react';

import SearchApp from './search/SearchApp';
import {Container} from './HomeStyles'
import useScrollTop from "../../hooks/useScrollTop";

const HomeScreen = () => {

    useScrollTop();

    return (
    <Container>
      <SearchApp/>
    </Container>
  );
};

export default HomeScreen;
