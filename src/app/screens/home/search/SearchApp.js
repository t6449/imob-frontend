import React, { useEffect, useState } from 'react';
import { useForm } from '../../../hooks/useForm';
import { Button, Col, Input, Row, Select } from 'antd';
import { getClassificationsWidthPropertyTypes } from '../HomeService';
import { SearchContainer } from './SearchStyles';
import {Link, useHistory} from "react-router-dom";
import uuid from "react-uuid";
import {alertUnimplemented} from "../../../shared/alerts/SweetAlerts";
import {useDispatch, useSelector} from "react-redux";
import {searchHomeAction} from "../redux/actions";

const SearchApp = () => {

  const history = useHistory();

  const dispatch = useDispatch();
  const { text } = useSelector((state) => state.home);

  const initialState = {
    searchText: ''
  };

  const [form, handleInputChange] = useForm(initialState);
  const { searchText } = form;

  const storeValue = (text) => {
    dispatch(searchHomeAction(text))
  };

  const { Option, OptGroup } = Select;
  const [classificationsWith, setClassificationsWith] = useState([]);

  const handleChange = (value) => {
  };
  useEffect(
    () => {
      getClassificationsWidthPropertyTypes()
        .then(
          value => {
            setClassificationsWith(value.data);
          }
        ).catch(error => {
          error ? console.error(error) : console.error('Could not load classifications correctly.');
        }
      );
    },
    []
  );

  return (
    <SearchContainer>

      {/*Header*/}
      <Row gutter={[4, 8]}>
        <Col>
          <Button
              size={'large'}
              onClick={alertUnimplemented}
          >Comprar</Button>
        </Col>
        <Col>
          <Button
              size={'large'}
              onClick={alertUnimplemented}
          >Alquilar</Button>
        </Col>
        <Col>
          <Select
            defaultValue='departamentos'
            onChange={handleChange}
            size={'large'}
          >
            {classificationsWith.map((classification) => (
              <OptGroup label={classification.name} key={classification.id}>
                {
                  classification?.propertyTypes.map((propertyType) => (
                    <Option
                        value={propertyType.name}
                        key={uuid()}
                        disabled={true}
                    >{propertyType.name}</Option>
                  ))
                }
              </OptGroup>
            ))}
          </Select>
        </Col>
      </Row>

      {/*Search*/}
      <Row gutter={[4, 8]} id="searchBar">
        <Col span={24}>
          <Input.Search
            defaultValue={text}
            placeholder='Ingresa Ubicacion, Oficina, ID'
            loading={false}
            size={'large'}
            enterButton={true}
            onSearch={(e) => {
              alertUnimplemented();
              storeValue(e)
            }}
          />
        </Col>
      </Row>

      {/*Footer*/}
      <Row gutter={[4, 8]}>
        <Col>
          <Button
              size={'large'}
              onClick={alertUnimplemented}
          >Ver busqueda avanzada</Button>
        </Col>
        <Col id="inventory">
          <Button type="primary" danger size={'large'} onClick={()=> history.push("/inventory")}>Consulta todo el inventario</Button>
        </Col>
      </Row>

    </SearchContainer>

  );
};

export default SearchApp;
