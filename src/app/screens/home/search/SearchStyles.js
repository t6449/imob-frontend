import styled from 'styled-components';

export const SearchContainer = styled.div`
  padding: 15px 0;
  
  #searchBar {
    max-width: 650px;
  }
  
  .ant-select {
    width: 150px!important;
  }

  .ant-row {
      margin-top: 4px!important;
  }

  .ant-btn-dangerous.ant-btn-primary {
      color: #fff;
      border-color: dimgrey!important;
      background: dimgrey!important;
  }

  @media screen and (min-width: 576px) {
    padding: 15px 5%;
  }
  
`;

