import { get } from '../../shared/backend/BackendService';
import {ENDPOINT_CLASSIFICATIONS} from "../../shared/backend/Constants";

export const getClassifications =  () => {
  return  get(ENDPOINT_CLASSIFICATIONS);
};

export const getClassificationsWidthPropertyTypes =  () => {
  return  get(ENDPOINT_CLASSIFICATIONS + '/with-property-types');
};
