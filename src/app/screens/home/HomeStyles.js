import styled from 'styled-components';
import img from './HomeBackground-3.jpg'

export const Container = styled.div`
  background: url(${img}) no-repeat center;
  background-size: cover;
  
  height: 100%;
  padding: 15px 5px;
  
  @media screen and (min-width: 576px) {
    padding: 15px 10%;
  }

  ::before {
    content: '';
    position: absolute;
    top: 0;
    left: 0;
    
    right: 0;
    bottom: 0;
    background: black;
    box-shadow: 0 0 36px 30px black;
    -webkit-transform: scale(.7);
    -ms-transform: scale(.7);
    transform: scale(1);
    opacity: 0.25;
    z-index: 0;
  }
`;

