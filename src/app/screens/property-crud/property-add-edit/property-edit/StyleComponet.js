import styled from 'styled-components';
import {ContainerBase} from "../shared/StyleComponetBase";

export const Container = styled(ContainerBase)`
  #oldImage {
    margin-right: 10px !important;
  }
  .image-container {
    display: flex;
    justify-content: center;
    align-items: center;
    #oldImage img {
      height: auto;
    }
  }
    
`;

