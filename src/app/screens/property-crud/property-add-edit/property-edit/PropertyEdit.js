import React, {useEffect, useState} from 'react';
import {Container} from "./StyleComponet";
import {useParams} from "react-router-dom/cjs/react-router-dom";
import {getPropertyById, updateProperty} from "../../PropertyService";
import {toastError, toastSuccess} from "../../../../shared/alerts/SweetToasts";
import useScrollTop from "../../../../hooks/useScrollTop";
import AddEditComponent from "../AddEdit";

export const PropertyEdit = () => {

    useScrollTop();

    const [form, setForm] = useState({});

    let {id} = useParams();

    useEffect(() => {
        getPropertyById(id)
            .then(response => {
                setForm({
                    ...form,
                    id,
                    title: response?.data?.title,
                    address: response?.data?.address,
                    surface: response?.data?.surface,
                    description: response?.data?.description,
                    fileId: response?.data?.fileId
                });
            })
            .catch(reason => console.error(reason));
    }, [])

    const handleSubmit = (formUpdated) => {
        updateProperty(id, formUpdated)
            .then(_ => { toastSuccess("Propiedad actualizada.")} )
            .catch(_ => toastError("Ocurrio un error al actualizar la propiedad."));
    };

    return (
        <Container>
            <AddEditComponent
                initialForm={ form }
                onSubmit= { handleSubmit }
                title= "Actulizar Propiedad"
            />
        </Container>
    );

}
