import styled from 'styled-components';
import {ContainerBase} from "../shared/StyleComponetBase";

export const Container = styled(ContainerBase)`
  height: 100vh;
  .ant-upload-list-item-card-actions-btn.ant-btn-sm {
    visibility: hidden;
  }
`;

