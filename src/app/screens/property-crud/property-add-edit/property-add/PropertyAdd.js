import React, {useState} from 'react';
import {createProperty} from "../../PropertyService";
import {toastError, toastSuccess} from "../../../../shared/alerts/SweetToasts";
import useScrollTop from "../../../../hooks/useScrollTop";
import AddEditComponent from "../AddEdit";

export const PropertyAdd = () => {


    useScrollTop();

    const initialForm = {
        title: '',
        address: '',
        surface: '',
        description: '',
        fileId: ''
    };

    const [form, setForm] = useState(initialForm);

    const handleSubmit = (formChanged) => {
        setForm(formChanged);
        createProperty(formChanged)
            .then(_ => {
                toastSuccess("Propiedad creada.");
                setForm(initialForm); // reset form
            } )
            .catch(_ => toastError("Ocurrio un error al crear la propiedad."));
    };


    return (
        <AddEditComponent
            title= "Nueva Propiedad"
            initialForm={ form }
            onSubmit={ handleSubmit }
        />
    );

}
