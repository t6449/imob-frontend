import {toastError} from "../../../../shared/alerts/SweetToasts";

export function beforeUploadCheckIsValidFile(file) {
    const isImage =
        file.type === 'image/jpeg' ||
        file.type === 'image/png' ||
        file.type === 'image/webp';
    if (!isImage) {
        toastError('El formato del archivo debe ser JPG, PNG o WEBP.');
        return false;
    }
    const isLt5M = file.size / 1024 / 1024 < 5;
    if (!isLt5M) {
        toastError('El tamaño del archivo debe ser menor a 5MB.');
        return false
    }
    return true;
}
