import styled from 'styled-components';

export const ContainerBase = styled.div`
  padding: 20px 5px;
  .ant-form-item {
    margin-bottom: 20px;
  }

  .ant-form-large .ant-form-item-label > label {
    height: 20px;
  }

  .btn-footer{
    margin-right: 5px;
  }

  @media screen and (min-width: 768px) {
    display: flex;
    flex-direction: column;
    justify-content: flex-start;
    align-items: center;
    #propertyForm{
      width: 768px;
    }
  }
  
`;

