import React, {useEffect, useState} from 'react';
import useScrollTop from "../../../../hooks/useScrollTop";
import {ENDPOINT_FILES} from "../../../../shared/backend/Constants";
import {PlusOutlined} from "@ant-design/icons";
import {Modal, Upload} from "antd";
import {beforeUploadCheckIsValidFile} from "./UploadCommons";

export const ImageUploader = ({ onUploadedImage, initialFileList = [] }) => {

    useScrollTop();

    const [previewImage, setPreviewImage] = useState('');
    const [previewTitle, setPreviewTitle] = useState('');
    const [previewVisible, setPreviewVisible] = useState(false);

    const [fileList, setFileList] = useState([]);

    function getBase64(file) {
        return new Promise((resolve, reject) => {
            const reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = () => resolve(reader.result);
            reader.onerror = (error) => reject(error);
        });
    }
    const handleCancel = () => setPreviewVisible(false);

    const handlePreview = async (file) => {
        if (!file.url && !file.preview) {
            file.preview = await getBase64(file.originFileObj);
        }
        setPreviewImage(file.url || file.preview);
        setPreviewVisible(true);
        setPreviewTitle(file.name || file.url.substring(file.url.lastIndexOf('/') + 1));
    };


    const handleChange = info => {

        setFileList(info.fileList);

        if (info.file.status === 'removed') {
            onUploadedImage({ fileId: '' })
        }

        if (info.file.status === 'done') {
            onUploadedImage({ fileId: info.file.response.data.hash })
        }

    };

    useEffect(() => {
        setFileList(initialFileList);
    }, [initialFileList])

    const uploadButton = (
        <div>
            <PlusOutlined />
            <div style={{ marginTop: 8 }}>Upload</div>
        </div>
    );

    return (
        <>

            <Upload
                action={ENDPOINT_FILES}
                listType="picture-card"
                fileList={fileList}
                onPreview={handlePreview}
                onChange={handleChange}
                maxCount={1}
                beforeUpload={beforeUploadCheckIsValidFile}
            >
                {fileList.length >= 8 ? null : uploadButton}
            </Upload>
            <Modal
                visible={previewVisible}
                title={previewTitle}
                footer={null}
                onCancel={handleCancel}
            >
                <img alt="example" style={{ width: '100%' }} src={previewImage} />
            </Modal>

        </>
    );

}
