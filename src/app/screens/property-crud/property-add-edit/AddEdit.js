import React, {useEffect} from 'react';
import {Container} from "./property-edit/StyleComponet";
import {Button, Form, Input} from "antd";
import {useForm} from "../../../hooks/useForm";
import {ImageUploader} from "./shared/ImageUploader";
import {ENDPOINT_FILES} from "../../../shared/backend/Constants";

const AddEditComponent = ({ title, initialForm, onSubmit}) => {

    const [form, handleInputChange, resetForm, setForm] = useForm( initialForm );

    // Form validation
    function formIsValid() {
        return form.title !== "" && form.fileId !== "";
    }

    const handleUploadedImage = ( data ) => {
        setForm({
            ...form, fileId: data.fileId
        })
    }

    // Hook to load data for update.
    useEffect(() => {
        setForm(initialForm)
    }, [initialForm])


    function getFileList() {
        if (form.fileId) {
            return [{uid: form.fileId, url: ENDPOINT_FILES + `/${form.fileId}`}];
        }
        return [];
    }

    return (
        <Container>
            <h1>{ title }</h1>
            <Form
                onFinish={() => onSubmit(form) }
                layout="vertical"
                size="large"
                id="propertyForm"
            >
                <ImageUploader
                    onUploadedImage={ handleUploadedImage }
                    initialFileList={ getFileList() }
                />
                <Form.Item
                    label="Título *"
                >
                    <Input
                        value={form.title}
                        onChange={handleInputChange}
                        name="title"

                    />
                </Form.Item>

                <Form.Item
                    label="Dirección"
                >
                    <Input
                        value={form.address}
                        onChange={handleInputChange}
                        name="address"
                    />
                </Form.Item>

                <Form.Item
                    label="Superficie"
                >
                    <Input
                        name="surface"
                        value={form.surface}
                        onChange={handleInputChange}
                    />
                </Form.Item>

                <Form.Item
                    label="Descripción"
                >
                    <Input.TextArea
                        name="description"
                        value={form.description}
                        onChange={handleInputChange}
                        rows={4}
                    />
                </Form.Item>

                <Form.Item>
                    <Button
                        type='primary'
                        htmlType="submit"
                        size={'large'}
                        disabled={!formIsValid()}
                        className="btn-footer"
                    >
                        Confirmar
                    </Button>
                </Form.Item>

            </Form>

        </Container>
    );
};

export default AddEditComponent;
