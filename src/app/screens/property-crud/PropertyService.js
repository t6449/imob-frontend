import {get, post, put, delete_} from '../../shared/backend/BackendService';
import {ENDPOINT_PROPERTIES} from "../../shared/backend/Constants";

export const getAllProperties =  () => {
  return  get(ENDPOINT_PROPERTIES);
};

export const getPropertyById =  (id = 0) => {
  return  get(ENDPOINT_PROPERTIES + `/${id}`);
};

export const deleteProperty =  (id = 0) => {
  return  delete_(ENDPOINT_PROPERTIES + `/${id}`);
};

export const getPage = ( page = 0,  filters = {}) => {
  return get(
      ENDPOINT_PROPERTIES + '/page?' + `page=${page}&searchText=${filters.searchText}&size=${filters.size}`
  );
}

export const createProperty =  ( body = {} ) => {
  return  post(ENDPOINT_PROPERTIES, body);
};

export const updateProperty =  (id , body = {} ) => {
  return  put(ENDPOINT_PROPERTIES + `/${id}`, body);
};
