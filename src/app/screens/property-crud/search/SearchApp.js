import React, {useEffect} from 'react';
import {Col, Input, Row} from 'antd';
import {SearchContainer} from './SearchStyles';
import {useDispatch, useSelector} from "react-redux";
import {fetchPageAction} from "../redux/actions";

const {Search} = Input;

const SearchApp = () => {

    const { filters } = useSelector((state) => state.propertyCRUD);
    const dispatch = useDispatch();

    const handlerSearch = (text) => {
        dispatch(fetchPageAction(
            0,
            { ...filters, searchText: text }
        ));
    }

    return (
        <SearchContainer>
            <Row gutter={[4, 8]}>
                <Col span={24}>
                    <Search
                        placeholder='Ingresa Ubicacion, Oficina, ID'
                        loading={false}
                        size={'large'}
                        enterButton={true}
                        onSearch={handlerSearch}
                        defaultValue={filters.searchText}
                    />
                </Col>
            </Row>
        </SearchContainer>

    );
};

export default SearchApp;
