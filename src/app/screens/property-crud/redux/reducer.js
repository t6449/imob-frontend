import {
    CREATE_PROPERTY_FAIL_CRUD,
    CREATE_PROPERTY_REQUEST_CRUD, CREATE_PROPERTY_SUCCESS_CRUD,
    DELETE_PROPERTY_FAIL_CRUD,
    DELETE_PROPERTY_REQUEST_CRUD,
    DELETE_PROPERTY_SUCCESS_CRUD,
    FETCH_PAGE_FAIL_CRUD,
    FETCH_PAGE_REQUEST_CRUD,
    FETCH_PAGE_SUCCESS_CRUD,
} from "./actionTypes";

export const stateDefault = {
    page: {
        content: [],
        totalElements: 0,
        size: 0,
        number: 0,
    },
    loading: false,
    filters: {
        searchText: '',
        size: 10
    },
    messageSuccess: '',
    messageError: '',
}
export const propertyCRUDReducer = (state = stateDefault, action) => {
    switch (action.type) {
        case DELETE_PROPERTY_REQUEST_CRUD:
            return {...state, loading: true};

        case DELETE_PROPERTY_SUCCESS_CRUD:
            return {...state, loading: false};

        case DELETE_PROPERTY_FAIL_CRUD:
            return {...state, loading: false};

        case FETCH_PAGE_REQUEST_CRUD:
            return {...state, loading: true};

        case FETCH_PAGE_SUCCESS_CRUD:
            return {...state, page: action.payload, loading: false, filters: action.filters};

        case FETCH_PAGE_FAIL_CRUD:
            return {...state, loading: false};

        case CREATE_PROPERTY_REQUEST_CRUD:
            return {...state, loading: true};

        case CREATE_PROPERTY_SUCCESS_CRUD:
            return {...state, loading: false, messageSuccess: action.message};

        case CREATE_PROPERTY_FAIL_CRUD:
            return {...state, loading: false, messageError: action.message};

        default:
            return state;
    }
};
