import {
    CREATE_PROPERTY_FAIL_CRUD,
    CREATE_PROPERTY_REQUEST_CRUD, CREATE_PROPERTY_SUCCESS_CRUD,
    DELETE_PROPERTY_FAIL_CRUD,
    DELETE_PROPERTY_REQUEST_CRUD,
    DELETE_PROPERTY_SUCCESS_CRUD,
    FETCH_PAGE_FAIL_CRUD,
    FETCH_PAGE_REQUEST_CRUD,
    FETCH_PAGE_SUCCESS_CRUD
} from "./actionTypes";
import {createProperty, deleteProperty, getPage} from "../PropertyService";

export const fetchPageAction = ( page = 0, filters = {}) => async (dispatch) => {
    try {
        dispatch({ type: FETCH_PAGE_REQUEST_CRUD });
        const { data } = await getPage(page, filters);
        dispatch({
            type: FETCH_PAGE_SUCCESS_CRUD,
            payload: data,
            filters
        });
    } catch (error) {
        dispatch({
            type: FETCH_PAGE_FAIL_CRUD,
            payload: error
        });
        console.error(error);
    }
};

export const deletePropertyAction = ( id = 0 ) => async (dispatch) => {
    try {
        dispatch({ type: DELETE_PROPERTY_REQUEST_CRUD });
        await deleteProperty(id);
        dispatch({
            type: DELETE_PROPERTY_SUCCESS_CRUD
        });
    } catch (error) {
        dispatch({
            type: DELETE_PROPERTY_FAIL_CRUD,
            payload: error
        });
        console.error(error);
    }
};

export const createPropertyAction = ( data = {} ) => async (dispatch) => {
    try {
        dispatch({ type: CREATE_PROPERTY_REQUEST_CRUD });
        await createProperty(data);
        dispatch({
            type: CREATE_PROPERTY_SUCCESS_CRUD,
            messageSuccess: 'Propiedad Creada.'
        });
    } catch (error) {
        dispatch({
            type: CREATE_PROPERTY_FAIL_CRUD,
            messageError: 'No se pudo crear la propiedad.'
        });
        console.error(error);
    }
};
