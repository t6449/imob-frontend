import React from 'react';
import {PropertyList} from './property-list/PropertyList';
import {Container} from "./StyleComponet";
import useScrollTop from "../../hooks/useScrollTop";
import {useSelector} from "react-redux";

const PropertyScreen = () => {

    const { page } = useSelector((state) => state.propertyCRUD);

    useScrollTop( () => {} , [page] );

    return (
        <Container>
            <PropertyList/>
        </Container>
    );
};
export default PropertyScreen;
