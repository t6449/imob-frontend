import {Button, Space, Table} from 'antd';
import React, {useEffect} from 'react';
import {Container} from "./StyleComponet";
import {Link, useHistory} from "react-router-dom";
import {DeleteTwoTone, EditTwoTone} from "@ant-design/icons";
import swal from "sweetalert";
import {useDispatch, useSelector} from "react-redux";
import {deletePropertyAction, fetchPageAction} from "../redux/actions";
import SearchApp from "../search/SearchApp";


export const PropertyList = () => {

    const dispatch = useDispatch();
    const {page, loading, filters} = useSelector((state) => state.propertyCRUD);

    const history = useHistory();

    const handleDelete = (id) => {
        swal({
            title: "¿Estas seguro que desea eliminar la propiedad?",
            text: "",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then(async (willDelete) => {
                if (willDelete) {
                    await dispatch(deletePropertyAction(id));
                    swal("Propiedad eliminada.", {
                        icon: "success",
                    });
                    dispatch(fetchPageAction(page.number, {...filters, searchText: filters.searchText}));
                }
            });
    }

    const columns = [
        {
            title: 'Título',
            dataIndex: 'title',
            key: 'title',
        },
        {
            title: 'Dirección',
            dataIndex: 'address',
            key: 'address',
            responsive: ['sm'], // show in small size (>576) or bigger
        },
        {
            title: 'Superficie',
            dataIndex: 'surface',
            key: 'surface',
        },
        {
            title: 'Acción',
            key: 'action',
            render: (text, record) => (
                <Space size="middle">
                    <Link to={`/properties/edit/${record.id}`}>
                        <EditTwoTone/>
                    </Link>
                    <Button
                        type="link"
                        onClick={() => handleDelete(record.id)}
                    >
                        <DeleteTwoTone twoToneColor="#eb2f96"/>
                    </Button>
                </Space>
            ),
        },
    ];

    const handleTableChange = (pagination) => {
        dispatch(fetchPageAction(pagination.current - 1, {...filters}));
    };

    useEffect(() => {
        dispatch(fetchPageAction(page.number, {...filters, searchText: filters.searchText}));
    }, [])

    return (
        <Container id="propertyList">
            <h1>ABM Propiedades</h1>
            <SearchApp/>
            <Table
                columns={columns}
                dataSource={page.content}
                rowKey="id"
                pagination={ {current: page.number + 1, pageSize: page.size, total: page.totalElements} }
                loading={loading}
                onChange={handleTableChange}
            />
            <Button
                type="primary"
                size={'large'}
                onClick={() => history.push("/properties/add")}
                id="addBtn"
            >
                +
            </Button>
        </Container>
    );

}
