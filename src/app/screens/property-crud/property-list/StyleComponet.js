import styled from 'styled-components';

export const Container = styled.div`
  
  padding: 15px 5px;
  text-align: center;
  
  .ant-pagination {
    position: fixed;
    bottom: 0;
    display: flex;
    justify-content: center;
    margin: 0 auto;
    padding: 12px;
    left: 0;
    right: 0;
    background-color: rgba(220,220,220, 0.75);
  }
  
  #addBtn {
    margin: 10px 25px;
    position: fixed;
    bottom: 0;
    right: 0;
    border-radius: 25px;
    font-size: 1.25rem;
    font-weight: bolder;
    display: flex;
    justify-content: center;
    align-items: center;
  }
  
  .ant-table {
    overflow: auto;
  }
  
  .ant-spin-nested-loading {
    margin-bottom: 70px;
  }
  
  table .anticon {
    font-size: 1.75em;
  }
  
  @media screen and (min-width: 1200px) {
    padding: 40px 10%;
    #addBtn {
      bottom: 60px;
      right: 60px;
    }
  }

`;

