import styled from 'styled-components';

export const Container = styled.div`
    padding: 12px;
    position: fixed;
    bottom: 0;
    left: 0;
    background-color: rgba(220,220,220, 0.75);
    width: 100%;
    .ant-pagination {
      text-align: center;
    }
`;
