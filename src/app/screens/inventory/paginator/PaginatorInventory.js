import {Pagination} from 'antd';
import React from 'react';
import "./PaginatorStyles.js";
import {Container} from './PaginatorStyles.js';
import {useDispatch, useSelector} from "react-redux";
import {fetchPageAction} from "../redux/actions";
import useScrollTop from "../../../hooks/useScrollTop";

function PaginatorInventory({show = true}) {

    const { page, filters } = useSelector((state) => state.inventory);
    const { number, totalElements, size} = page;
    const dispatch = useDispatch();
    const handleChangePage = (pageNumber) => {
        dispatch(fetchPageAction(pageNumber - 1, {
            ...filters,
            searchText: filters.searchText
        }));
    }

    return (
        <Container>
            { show &&
                <Pagination
                    current={number + 1}
                    total={totalElements}
                    onChange={handleChangePage}
                    pageSize={size}
                    size="large"
                    locale={{items_per_page: ' / Página'}}
                    showLessItems={true}
                />
            }
        </Container>
    )
}

export default PaginatorInventory
