import styled from 'styled-components';

export const Container = styled.div`
  .ant-card-body {
    padding: 0;
  }

  @media screen and (min-width: 576px) {
    .ant-card-body {
      padding: 20px;
    }
  }

`;

export const ImageContainer = styled.div`
  overflow: hidden;
  img {
    width: 100%;
  }
`;

export const TextContainer = styled.div`
  padding: 5px;
  word-break: break-word;
  
  #title{
  font-size: 1.5em;
  }
  #address{
      color: gray;
  }
  #surface{
      color: gray;
      margin: 5px 0;
  }
`;

