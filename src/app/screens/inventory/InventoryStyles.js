import styled from 'styled-components';

export const Container = styled.div`
  background: lightgray;
  padding: 15px 5px;
  
  #spacerPaginator {
    height:  45px;
  }

  @media screen and (min-width: 576px) {
    padding: 45px 10%;
  }
  
`;

