import {FETCH_PAGE_FAIL, FETCH_PAGE_REQUEST, FETCH_PAGE_SUCCESS} from "./actionTypes";
import {getPage} from "../InventoryService";

export const fetchPageAction = ( page = 0, filters = {}) => async (dispatch) => {
    try {
        dispatch({ type: FETCH_PAGE_REQUEST });
        const { data } = await getPage(page, filters);
        dispatch({
            type: FETCH_PAGE_SUCCESS,
            payload: data,
            filters
        });
    } catch (error) {
        dispatch({
            type: FETCH_PAGE_FAIL,
            payload: error
        });
        console.error(error);
    }
};
