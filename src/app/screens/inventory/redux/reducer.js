import {FETCH_PAGE_FAIL, FETCH_PAGE_REQUEST, FETCH_PAGE_SUCCESS} from "./actionTypes";

export const stateDefault = {
    page: {
        content: [],
        totalElements: 0,
        size: 0,
        number: 0,
    },
    loading: false,
    filters: {
        searchText: '',
        size: 6
    }
}
export const inventoryReducer = (state = stateDefault, action) => {
    switch (action.type) {
        case FETCH_PAGE_REQUEST:
            return {...state, loading: true};
        case FETCH_PAGE_SUCCESS:
            return {...state, page: action.payload, loading: false, filters: action.filters};
        case FETCH_PAGE_FAIL:
            return {...state, loading: false};
        default:
            return state;
    }
};
