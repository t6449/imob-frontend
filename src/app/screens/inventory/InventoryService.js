import {get} from "../../shared/backend/BackendService";
import {ENDPOINT_FILES, ENDPOINT_PROPERTIES} from "../../shared/backend/Constants";

export const getPropertyById =  (id) => {
    return  get(ENDPOINT_PROPERTIES + `/${id}`);
};

export const getFileById =  (id) => {
    return  get(ENDPOINT_FILES + `/${id}`);
};

export const getAllProperties =  () => {
    return  get(ENDPOINT_PROPERTIES);
};

export const getPage = ( page = 0,  filters = {}) => {
    return get(
        ENDPOINT_PROPERTIES + '/page?' + `page=${page}&searchText=${filters.searchText}&size=${filters.size}`
    );
}

