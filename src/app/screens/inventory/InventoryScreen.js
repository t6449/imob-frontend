import React, {useEffect, useState} from 'react';

import {Container} from "./InventoryStyles";
import SearchApp from "./search/SearchApp";
import Loading from "../../shared/loading/Loading";
import {Col, Row} from "antd";
import PaginatorImob from "./paginator/PaginatorInventory";
import CardApp from "./card/CardApp";
import {useDispatch, useSelector} from "react-redux";
import {fetchPageAction} from "./redux/actions";
import useScrollTop from "../../hooks/useScrollTop";

const InventoryScreen = () => {

    const dispatch = useDispatch();
    const { page, loading, filters} = useSelector((state) => state.inventory);
    useScrollTop();

    useEffect(() => {
        dispatch(fetchPageAction(page.number, { ...filters, searchText: filters.searchText}));
    }, [])

    return (
        <Container>
            <Loading show={loading}/>
            {!loading &&
            <>
                <SearchApp/>
                <Row gutter={[28, 28]}>
                    {page.content.map(p =>
                        <Col
                            xs={24} sm={12}
                            key={p.id}
                        >
                            <CardApp key={p.id} property={p}/>
                        </Col>
                    )}
                </Row>
                <div id="spacerPaginator"/>
                <PaginatorImob show={!loading}/>
            </>
            }
        </Container>
    );
};

export default InventoryScreen;
