import './App.css';
import Router from './app/router/router';
import {Toaster} from "react-hot-toast";

function App() {
  return (
      <>
        <Router />
        <Toaster />
      </>
  );
}

export default App;
