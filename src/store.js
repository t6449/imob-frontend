import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';
import {inventoryReducer} from "./app/screens/inventory/redux/reducer";
import {homeReducer} from "./app/screens/home/redux/reducer";
import {propertyCRUDReducer} from "./app/screens/property-crud/redux/reducer";

const appReducer = combineReducers({
  inventory: inventoryReducer,
  home: homeReducer,
  propertyCRUD: propertyCRUDReducer
});

const initialState = {
};

const rootReducer = (state, action) => {
  return appReducer(state, action);
};

const middleware = [thunk];

const store = createStore(rootReducer, initialState, composeWithDevTools(applyMiddleware(...middleware)));

export default store;
